#[allow(unused_imports)]
use anyhow::Result;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{
    CreateCollection, SearchPoints, VectorParams, VectorsConfig,
};

use serde_json::json;
use serde_json::Value;
mod aggregate;
mod visualization;

use crate::aggregate::aggregate;
use crate::visualization::visualize;

#[tokio::main]
async fn main() -> Result<()> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;

    let collection_name = "my_collection";
    client.delete_collection(collection_name).await?;

    client
        .create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 10,  // This should match the dimensions of your actual vectors
                    distance: Distance::Cosine.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;

    // Adapted part: creating a payload from the courses data
    let courses_data = vec![
        ("Data Analytics", 96),
        ("Data Engineering", 93),
        ("Machine Learning", 94),
        ("Cloud Computing", 97),
        ("Data Structures", 95),
    ];

    let courses_payload: Value = courses_data
        .into_iter()
        .map(|(name, score)| (name.to_string(), json!(score)))
        .collect::<serde_json::Map<_, _>>().into();

    let payload: Payload = json!({
        "courses": courses_payload
    })
    .try_into()
    .unwrap();

    let points = vec![PointStruct::new(0, vec![12.; 10], payload)]; // Ensure vector size matches the specified size in the collection schema
    client
        .upsert_points_blocking(collection_name, None, points, None)
        .await?;

    // Example search, adapted to potentially look for a course by name
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![12.; 10], // Your actual search vector here
            //filter: Some(Filter::all([Condition::matches("Data Analytics", 96)])), // Example adapted filter
            limit: 10,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;
    
    // Transform data from search result
    let found_point = search_result.result.into_iter().next().unwrap();
    let mut payload = found_point.payload;
    let courses_payload = payload.remove("courses").unwrap().into_json();
    //println!("courses: {}", courses_payload);

    // Call aggregate and pass courses fetched
    let aggregated_results = aggregate(courses_payload);

    //print aggregated results
    //println!("{:?}", aggregated_results);
    
    // Call visualization function to visualize the results.
    visualize(aggregated_results);
 

    Ok(())
}
