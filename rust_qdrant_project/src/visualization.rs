use prettytable::{Table, Row, Cell};
use crate::aggregate::CourseGrade;

pub fn visualize(course_grades: Vec<CourseGrade>) {
    let mut table = Table::new();

    // Manually constructing the header row without using the `row!` macro
    let header = Row::new(vec![
        Cell::new("Course"),
        Cell::new("Score"),
        Cell::new("Grade"),
    ]);
    table.add_row(header);

    for grade in course_grades {
        let row = Row::new(vec![
            Cell::new(&grade.name),
            Cell::new(&grade.score.to_string()),
            Cell::new(&grade.grade),
        ]);
        table.add_row(row);
    }

    //print table
    table.printstd();
}