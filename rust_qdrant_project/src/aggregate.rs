use serde_json::Value;

#[derive(Debug, Clone)]
pub struct CourseGrade {
    pub name: String,
    pub score: i64,
    pub grade: String,
}

pub fn assign_grade(score: i64) -> String {
    match score {
        95..=100 => "A",
        90..=94 => "B",
        85..=89 => "C",
        _ => "D",
    }
    .to_string()
}

pub fn aggregate(courses_payload: Value) -> Vec<CourseGrade> {
    let courses_map = courses_payload.as_object().unwrap(); // Safely unwrap, assuming the structure is always correct
    let mut results: Vec<CourseGrade> = Vec::new();

    for (name, score_val) in courses_map {
        if let Some(score) = score_val.as_i64() {
            let grade = assign_grade(score);
            results.push(CourseGrade {
                name: name.clone(),
                score,
                grade,
            });
        }
    }

    results
}