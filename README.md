# Rust Qdrant Data Processing Project

This project is a Rust application that demonstrates how to ingest data into a Qdrant vector database, perform queries and aggregations, and visualize the results in a terminal table format.

## Features

- Data ingestion into Qdrant
- Data querying and aggregation
- Visualization of query results with pretty tables

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Rust Programming Language
- Cargo Package Manager
- Qdrant running locally or accessible remotely

## Installation and Setup

Clone the repository to your local machine:

```sh
git clone https://gitlab.com/Ofosu-Osei/rust_data_processing_with_vector_database.git
cd rust_data_processing_with_vector_database/rust_qdrant_project
```

Make sure Qdrant is running and accessible. The default URL is http://localhost:6334.

If you have Docker installed, you can run this command:

```sh
docker run -p 6333:6333 \
    -v $(pwd)/path/to/data:/qdrant/storage \
    -v $(pwd)/path/to/custom_config.yaml:/qdrant/config/production.yaml \
    qdrant/qdrant
```

## Usage

To run the project, use the following Cargo command:
```sh
cargo run
```
### Expected Ouput:

The application will connect to the Qdrant database, ingest predefined course data, calculate grades, and output a table with courses, scores, and grades to the terminal.

![output](rust_qdrant_project/img/output.png)

## Modules

- [main.rs](rust_qdrant_project/src/main.rs): The entry point of the application that coordinates the data processing flow.
- [aggregate.rs](rust_qdrant_project/src/aggregate.rs): Defines the logic for data aggregation and grade assignment.
- [visualization.rs](rust_qdrant_project/src/visualization.rs): Handles the visualization of aggregated data using pretty tables.

## Contributing to Rust Qdrant Data Processing Project

To contribute, follow these steps:

- Fork this repository.
- Create a branch: git checkout -b <branch_name>.
- Make your changes and commit them: git commit -m '<commit_message>'
- Push to the original branch: git push origin <project_name>/<location>
- Create the pull request.